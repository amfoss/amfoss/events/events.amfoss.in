import React from 'react';
import dataFetch from '../../../utils/dataFetch';
import photo from '../images/hacktober-amfoss.jpg';

class Closed extends React.Component {
  render() {
    return (
      <section id="registration-form">
        <div className="row m-0 d-flex justify-content-center w-100">
          <div
            style={{
              backgroundImage: `url(${photo})`,
              backgroundPosition: 'bottom',
              backgroundRepeat: 'no-repeat',
              objectFit: 'fill',
              backgroundSize: 'cover',
              minHeight: '60vh',
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
            }}
            className="col-md-7 p-0"
          ><div className="col-md-5 p-4 d-flex align-items-center justify-content-center">
          <h1 className="my-4 text-white text-center closed-text">
            REGISTRATIONS CLOSED!
          </h1>
        </div></div>
          
        </div>
      </section>
    );
  }
}
export default Closed;
