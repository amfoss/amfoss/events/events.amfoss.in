import React from 'react';

const ComingSoon = () => {
  return (
    <div className="" id="registration-form">
      <div className="m-5 px-5 comingSoon">
        <div className="comingSoonText">
          <p>REGISTRATIONS OPENING SOON!</p>
        </div>
        <div className="fakeForm">
          <h1 className="my-4 text-light">Register Now!</h1>
          <p className="text-light">
            Sign up for the meetup for free by filling up the form below, and
            make sure you do that fast as we have limited seats to fit you all
            in! Also, don't forget to bring in your friends as well :)
          </p>
          <br />
          <p style={{ color: 'red' }}>
            * Event only open for students of Amritapuri Campus.
          </p>
          <form className="form-group">
            <div className="row">
              <div className="col-12 py-2">
                <div className="m-2">
                  <input
                    type="text"
                    placeholder="Enter Full Name"
                    name="name"
                    className="form-control"
                  />
                </div>
              </div>
              <div className="col-sm-6 py-2">
                <div className="m-2">
                  <input
                    type="text"
                    placeholder="Enter Roll Number/Registration Number"
                    name="Roll No"
                    className="form-control"
                  />
                </div>
              </div>
              <div className="col-sm-6 py-2">
                <div className="m-2">
                  <select className="form-control text-dark">
                    <option defaultValue="Select Gender" hidden>
                      Select Gender
                    </option>
                    <option value="male">Male</option>
                    <option value="female">Female</option>
                  </select>
                </div>
              </div>
              <div className="col-12 py-2">
                <div className="m-2">
                  <input
                    type="text"
                    placeholder="Enter Email"
                    name="email"
                    className="form-control"
                  />
                </div>
              </div>
              <div className="col-sm-6 p-0">
                <div className="m-2" />
              </div>
              <div className="col-12 form-check">
                <div className="m-2 text-dark text-center d-flex justify-content-center">
                  <label className="form-check-label" htmlFor="undertaking">
                    By submitting this application, I agree to the Code of
                    Conduct of the organizers.
                  </label>
                </div>
              </div>
              <div className="mx-auto my-3 p-0 text-center text-md-right">
                <div className="m-2">
                  <button
                    type="submit"
                    className="button-new"
                    id="register-btn"
                    style={{ opacity: 0.2 }}
                  >
                    REGISTER
                  </button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
      <div className="smallWindowText">
        <h1>REGISTRATIONS OPENING SOON!</h1>
      </div>
    </div>
  );
};

export default ComingSoon;
