import React from 'react';

const DottedDivider = () => {
  return (
    <div className="dotted">
      <div className="dottedDivider"></div>
      <div className="dottedDivider"></div>
    </div>
  );
};

export default DottedDivider;
