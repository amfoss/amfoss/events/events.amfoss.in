import React from 'react';
import DigitalOcean from '../images/Logo.png';
import DottedDivider from './dottedDivider';
import PrizesSection from './prizesSection';

const LearnMore = () => {
  return (
    <section>
      <DottedDivider />
      <div
        className="flex-row m-0 d-flex align-items-center py-1 w-100"
        style={{
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
        }}
        id="learn-more"
      >
        <div className="col-3 learnmoreLogo">
          <img src={DigitalOcean} alt="Image" />
        </div>
        <div className="col-7 learnmoreText">
          <p className="text-light">
            Hacktoberfest is an annual event hosted by DigitalOcean and GitHub
            promoting and supporting Open Source collaboration. It's all about
            encouraging meaningful contributions to open source.
            <br />
            Join Amritapuri Hacktoberfest meetup hosted by amFOSS on 12th of
            October and don't miss this oppurtunity.
          </p>
        </div>
      </div>
      <PrizesSection />
      <div
        className="w-100 pb-5 pt-2 LearnMoreButtonHolder"
        style={{
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        <a
          href="https://hacktoberfest.digitalocean.com/"
          className="learnmoreButton"
        >
          <button className="button-new">LEARN MORE!</button>
        </a>
      </div>
      <DottedDivider />
    </section>
  );
};

export default LearnMore;
