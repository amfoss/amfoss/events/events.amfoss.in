import React, { useEffect, useState } from 'react';
import { Link } from 'react-scroll';

const CountdownNew = () => {
  const [days, setDays] = useState(0);
  const [hours, setHours] = useState(0);
  const [minutes, setMinutes] = useState(0);
  const [seconds, setSeconds] = useState(0);
  const [progress, setProgress] = useState(0);
  const deadline = 'October 12, 2023';
  const totalTime = 11;

  function getTimeUntil(deadline) {
    const time = Date.parse(deadline) - Date.parse(new Date());
    if (time < 0) {
      setDays(0);
      setHours(0);
      setMinutes(0);
      setSeconds(0);
    } else {
      setSeconds(Math.floor((time / 1000) % 60));
      setMinutes(Math.floor((time / 1000 / 60) % 60));
      setHours(Math.floor((time / (1000 * 60 * 60)) % 24));
      setDays(Math.floor(time / (1000 * 60 * 60 * 24)));
      setProgress(17 - Math.floor((days / totalTime) * 17));
    }
  }

  useEffect(() => {
    getTimeUntil(deadline);
    const timerInterval = setInterval(() => getTimeUntil(deadline), 1000);
    return () => {
      clearInterval(timerInterval);
    };
  }, [deadline, days]);

  const progressbarBlocks = Array.from({ length: 17 }, (_, index) => (
    <div
      key={index}
      className={`progressbar-block-${index < progress ? 'filled' : 'empty'}`}
    ></div>
  ));

  return (
    <div className="h-100 d-flex align-items-center justify-content-center">
      <div>
        <div className="d-flex align-items-center justify-content-center">
          <div className="text-center">
            <p className="text-white">
              Countdown for Hacktoberfest starts now!
            </p>
            <div className="row mt-2">
              <div className="col text-center">
                <h1>{days}</h1>
                <p className="text-white">Days</p>
              </div>
              <div className="col text-center">
                <h1>{hours}</h1>
                <p className="text-white">Hours</p>
              </div>
              <div className="col text-center">
                <h1>{minutes}</h1>
                <p className="text-white">Minutes</p>
              </div>
            </div>
            <div className="mt-2">
              {/* progress bar */}
              <div className="mt-2 progressbar">
                <div className="progressbar-background">
                  <svg viewBox="0 0 474 36" preserveAspectRatio="none">
                    <rect
                      x=".5"
                      y=".5"
                      width="473"
                      height="35"
                      rx="16"
                      fill="none"
                      stroke="currentColor"
                      strokeWidth="2"
                      strokeDasharray="6,6"
                      vectorEffect="non-scaling-stroke"
                    ></rect>
                  </svg>
                </div>
                <div className="progressbar-blocks">{progressbarBlocks}</div>
              </div>
            </div>
          </div>
        </div>
        <div className="d-flex justify-content-center align-items-center mt-5">
          <Link to="registration-form" smooth={true} duration={1500}>
            <button className="button-new">REGISTER NOW!</button>
          </Link>
        </div>
      </div>
    </div>
  );
};

export default CountdownNew;
