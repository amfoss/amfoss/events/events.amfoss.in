import React from 'react';
import Digitalocean from '../images/digitalocean.svg';
import Github from '../images/github.png';
import appwrite from '../images/appwrite.png';
import amFOSSLogo from '../../../images/amfoss_logo.png';
import bgFooter from '../images/bg-footer.svg';

const Footer = () => {
  return (
    <div className="footer">
      <div>
        <h1 className="p-4 text-light text-center">
          Support Open Source With
          <br />
          <br />
          <a
            href="https://amfoss.in"
            style={{ display: 'flex', justifyContent: 'center' }}
          >
            <img
              alt="amFOSS Logo"
              className="amFOSSLogo"
              src={amFOSSLogo}
              draggable="false"
              style={{ width: '180px', height: '35px' }}
            />
          </a>
        </h1>
      </div>
      <div className="text-center text-light">Thank You</div>
      <div id="footer-logos" className="row m-0">
        <div className="container my-2 d-flex justify-content-center">
          <a href="https://www.digitalocean.com" target="_blank">
            <img
              className="px-2 mt-1"
              src={Digitalocean}
              alt="Digital Ocean"
              title="Digital Ocean"
            />
          </a>
          <a href="https://github.com/" target="_blank">
            <img
              className="px-2 mt-1"
              src={Github}
              alt="Github"
              title="GitHub"
            />
          </a>
          <a href="https://appwrite.io/" target="_blank">
            <img
              className="px-2 mt-1"
              src={appwrite}
              alt="appwrite"
              title="Appwrite"
            />
          </a>
        </div>
      </div>
    </div>
  );
};

export default Footer;
