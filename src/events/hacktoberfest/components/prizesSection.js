import React from 'react';
import Keyboard from '../images/prize-keyboard.png';
import Speaker from '../images/prize-speaker.png';
import Tshirt from '../images/prize-tshirt.png';

const PrizesSection = () => {
  return (
    <div className="text-white">
      <div className="container mb-5 mt-sm-5">
        <h1 className="d-flex align-items-center justify-content-center mt-5 prizes-heading">
          GAME PRIZES
        </h1>
        <div className="row">
          <div className="col-md-4 col-sm-6 my-2 d-flex flex-column justify-content-end">
            <div className="d-flex justify-content-center">
              <img
                src={Keyboard}
                alt="Keyboard"
                className="w-100 prizes-images"
              />
            </div>
            <div className="stage"></div>
          </div>
          <div className="col-md-4 col-sm-6 mb-3 d-flex flex-column justify-content-end">
            <div className="d-flex justify-content-center align-items-end">
              <img
                src={Speaker}
                alt="Earbuds"
                className="w-100 noise-buds prizes-images"
              />
            </div>
            <div className="stage"></div>
          </div>
          <div className="col-md-4 col-sm-6 mb-3 d-flex flex-column justify-content-end">
            <div className="d-flex justify-content-center align-items-end">
              <img src={Tshirt} alt="T-shirt" className="w-100 prizes-images" />
            </div>
            <div className="stage"></div>
          </div>
        </div>
      </div>
      <div className="d-flex justify-content-center my-3 mx-5 px-5 prizes-content">
        <p className="text-light">
          Register now to be part of interactive sessions on subjects such as
          Linux and Terminal, open-source AI, making your initial contribution
          etc. Plus, participate in games like TypeRacer, Trivia Quizzes, and
          Fastest Contribution for a chance to win incredible rewards like a
          Logitech Keyboard, Github T-shirts, Noise Earpods, Swags and much
          more.
        </p>
      </div>
    </div>
  );
};

export default PrizesSection;
