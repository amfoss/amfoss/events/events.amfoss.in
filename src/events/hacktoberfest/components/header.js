import React from 'react';
import Logo from '../images/Logo-title.png';
import amFOSSLogo from '../../../images/amfoss_logo.png';
import amritaLogo from '../../../images/amrita_logo.png';
import Particles from 'react-particles-js';
import { Link } from 'react-scroll';
import scrollSvg from '../images/scroll.svg';
import { useState } from 'react';
import { useEffect } from 'react';
import CountdonwNew from './countdownNew';
import githubLogo from '../images/githubLogo.png';
import digitalOceanLogo from '../images/digitalOceanLogo.svg';
import appwriteLogo from '../images/appwriteLogo.svg';

const Header = () => {
  var s1 =
    '01001000 01100101 01101100 01101100 01101111 00101100 00100000 01010111 01100101 01101100 01100011 01101111 01101101 01100101 00100000 01110100 01101111 00100000 01100001 01101101 01000110 01001111 01010011 01010011 00100000 01101111 01110000 01100101 01101110 00100000 01110011 01101111 01110101 01110010 01100011 01100101 00100001';

  var s2 =
    '01001000 01100101 01101100 01101100 01101111 00100000 01010111 01100101 01101100 01100011 01101111 01101101 01100101 00100000 01110100 01101111 00100000 01001000 01100001 01100011 01101011 01110100 01101111 01100010 01100101 01110010 00100000 01100110 01100101 01110011 01110100 00100000 00110010 00110000 00110010 00110010 00100001';

  const [binaryMessage, setBinaryMessage] = useState(s1);

  useEffect(() => {
    const interval = setInterval(() => {
      const d = new Date();
      let seconds = d.getSeconds();
      if (seconds % 2 === 0) {
        setBinaryMessage(s1);
      } else {
        setBinaryMessage(s2);
      }
    }, 1000);
    return () => clearInterval(interval);
  }, []);

  return (
    <>
      <div id="header-area" className="headerSection">
        <Particles
          canvasClassName="particleBg"
          params={{
            particles: {
              number: {
                value: 150,
                density: {
                  enable: true,
                  value_area: 1500,
                },
              },
              size: {
                value: 2,
                random: false,
                anim: {
                  speed: 0.5,
                  size_min: 0.5,
                },
              },
              color: {
                value: '#64E3FF',
              },
              line_linked: {
                enable: false,
              },
              move: {
                random: false,
                speed: 0.8,
                direction: 'right',
                out_mode: 'out',
              },
            },
            interactivity: {
              events: {
                onhover: {
                  enable: true,
                  mode: 'push',
                },
              },
              modes: {
                push: {
                  particles_nb: 1,
                },
              },
            },
            retina_detect: true,
          }}
        />
        <div id="top-bar" className="row mx-4 py-3">
          <div className="col-6">
            <a href="https://amfoss.in">
              <img
                alt="amFOSS Logo"
                className="amFOSSLogo"
                src={amFOSSLogo}
                draggable="false"
              />
            </a>
          </div>
          <div className="col-6 text-right">
            <a href="https://www.amrita.edu/">
              <img
                alt="Amrita Logo"
                className="amritaLogo"
                src={amritaLogo}
                draggable="false"
              />
            </a>
          </div>
        </div>
        <div
          className="header-title mt-5 mt-md-0"
          style={{
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          <div
            className="m-0 row"
            style={{
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            <div className="col-md-4 mx-5 leftBoxHeader">
              <CountdonwNew />
            </div>
            <div className="col-md-7">
              <div className="text-center py-4">
                <img className="hacktoberLogo image-fluid" src={Logo} />
              </div>
              <div className="d-flex flex-wrap align-items-center justify-content-between text-white mx-3 presentedByComponent">
                <div className="d-flex justify-content-center w-100 align-items-center">
                  <span className="presentedText">PRESENTED BY:</span>
                </div>
                <div className="d-flex justify-content-center w-100 mb-3">
                  <a href="https://amfoss.in">
                    <img
                      alt="amFOSS Logo"
                      className="amFOSSLogo"
                      src={amFOSSLogo}
                      draggable="false"
                    />
                  </a>
                </div>
              </div>
              <br />
              <div className="d-flex flex-wrap align-items-center justify-content-center w-70">
                <div className="w-100">
                  <div className="text-center">
                    <h1>Amritapuri</h1>
                    <h3>October 12th</h3>
                  </div>
                </div>
              </div>
            </div>

            <div id="scroll-down">
              <Link
                to="learn-more"
                smooth={true}
                duration={1000}
                style={{ color: 'black', textDecoration: 'none' }}
              >
                <div className="lg:block hidden">
                  <img src={scrollSvg} id="scrollbtn"></img>
                </div>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Header;
