import React, { useState, useEffect } from 'react';
import './styles/style.sass';

import moment from 'moment';

import Header from './components/header';
import SEO from '../../components/seo';
import Countdown from './components/countdown';
import Registration from './components/registration';
import Footer from './components/footer';
import RSVPForm from './components/rsvpForm';
import LearnMore from './components/learnmore';
import Closed from './components/closed';
import ComingSoon from './components/comingSoon';

const Hacktoberfest = () => {
  var date1 = moment()
    .utcOffset('+05:30')
    .format('DD-MM-YYYY');
  var date2 = '12-10-2023';
  var date1Parsed = moment(date1, 'DD-MM-YYYY');
  var date2Parsed = moment(date2, 'DD-MM-YYYY');
  var display;

  const [hash, setHash] = useState(undefined);
  const [queryLoaded, setQueryLoaded] = useState(false);

  // if (date1Parsed.isBefore(date2Parsed)) {
  //   display = <Registration />;
  // } else {
  display = <Closed />;
  // }

  useEffect(() => {
    if (!queryLoaded) {
      const query = window.location.search.substring(1);
      const queryHash = query.split('=');
      setHash(queryHash[1]);
      setQueryLoaded(true);
    }
  });

  return (
    <>
      <SEO title="Hacktoberfest 2023 - Meetup & BootCamp | Amritapuri | October 12th" />
      {hash === undefined ? (
        <>
          <Header />
          <LearnMore />
          {display}
          <Footer />
        </>
      ) : (
        <RSVPForm hash={hash} />
      )}
    </>
  );
};

export default Hacktoberfest;
